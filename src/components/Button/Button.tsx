import { CSSProperties, memo, ReactElement } from 'react';
import './Button.scss';

export type InputProps = {
  onClick: () => void;
  children:ReactElement | string,
  style?:CSSProperties,
  className?:string
};

function Input({
  onClick,
  children,
  style={},
  className=""
}: InputProps): ReactElement {


  return (
    <button
    type="button"
      onClick={onClick}
      style={style}
      className={`Button ${className}`}
  >{children}</button>
  );
}

export default memo(Input);
