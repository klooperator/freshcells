import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import Button from './Button';

describe('Button component', () => {

  it('Should render properly',()=>{
    const soloButton = render(
        <Button onClick={():void=>{}}>TestButton</Button>,
      );
      expect(soloButton.container).toMatchSnapshot();
  });

  it('should be clickable',async ()=>{
    const fakeClick = jest.fn();
    const soloButton = render(
        <Button onClick={fakeClick}>TestButton</Button>,
      );
      fireEvent.click(soloButton.getByText('TestButton'));
      expect(fakeClick).toBeCalled();
  })

});
