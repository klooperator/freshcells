import React, { CSSProperties, memo, ReactNode, ReactElement } from 'react';
import './PageWrapper.scss';

export type PageWrapperProps = {
  style?: CSSProperties;
  className?: string;
  children: ReactNode | ReactNode[];
};

function PageWrapper({
  style,
  className,
  children,
  ...other
}: PageWrapperProps): ReactElement {
  return (
    <div className={`PageWrapper ${className}`} style={style} {...other}>
      {children}
    </div>
  );
}

export default memo(PageWrapper);
