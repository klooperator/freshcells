import { ChangeEvent, CSSProperties, memo, ReactElement } from 'react';
import './Input.scss';

export type InputProps = {
  onChange: (value: string) => void;
  placeholder?: string;
  value: string;
  type?: string;
  style?:CSSProperties,
  className?:string
};

function Input({
  onChange,
  value,
  type = 'text',
  placeholder = '',
  style={},
  className=""
}: InputProps): ReactElement {

  const parseChange = (event: ChangeEvent<HTMLInputElement>): void => {
    onChange(event.target.value);
  };

  return (
    <input
      value={value}
      onChange={parseChange}
      placeholder={placeholder}
      type={type}
      style={style}
      className={`Input ${className}`}
    />
  );
}

export default memo(Input);
