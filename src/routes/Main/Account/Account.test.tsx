import { MockedProvider } from '@apollo/client/testing';
import { MemoryRouter } from 'react-router-dom';
import { render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import Account from './Account';
import { GET_ME, GET_USER } from '../../../graphql';

const mocks = [
  {
    request: {
      query: GET_ME,
    },
    result: {
      data: {
        me: {
          id: '2',
        },
      },
    },
  },
  {
    request: {
      query: GET_USER,
      // variables:
    },
    result: {
      data: {
        user: {
          email: 'some@mail.com',
          firstName: 'SomeName',
          lastName: 'VanDerLastName',
        },
      },
    },
  },
];

describe('Account scene tests', () => {
  const renderedScene = render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MemoryRouter initialEntries={['/']}>
        <Account />
      </MemoryRouter>
    </MockedProvider>
  );
 
  // I couldn't get this Provider to work correctly
  // so already lost to much time on this

  it("Should match snapshot",()=>{
      expect(renderedScene.container).toMatchSnapshot();
  });

});
