import { useQuery } from '@apollo/client';
import React,{memo,ReactElement} from 'react';
import { Button, PageWrapper } from 'src/components';
import { GET_ME, GET_USER } from 'src/graphql';
import { useLogout } from 'src/utils';
import './Account.scss';




function Account():ReactElement {
    const {logout} = useLogout();
    const resMe = useQuery(GET_ME);
    
    let id='';
    if(resMe?.data?.me?.id)id=resMe.data.me.id;
    const userAll = useQuery(GET_USER,{variables:{id}, skip:!id});
    const {data} = userAll;
    if(!data?.user?.email)return <div>Loading</div>;
    const {user:{firstName, lastName}} = data;
return ( 
<PageWrapper className="Account">
    <strong>Hello:</strong>
    <div className="nameWrapper">
        <div>{firstName}</div>
        <div>{lastName}</div>
    </div>
    <Button onClick={logout}>Logout</Button>
</PageWrapper>
);
}




export default memo(Account);
