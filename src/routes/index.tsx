import {  useQuery } from '@apollo/client';
import React, { ReactElement, useEffect } from 'react';
import { Switch, Route,useHistory,useLocation } from 'react-router-dom';
import {GET_ME} from '../graphql';
import Login from './Auth/Login/Login';
import {ROUTES} from '../consts';
import Account from './Main/Account/Account';


export default function Routes():ReactElement {
    const history = useHistory();
    const location = useLocation();
    const {loading,error, data, refetch} = useQuery(GET_ME,{
        onError:(e):void=>{
            console.warn('ERROR',e);
        },
    });
    useEffect(()=>{
        if(location.pathname === ROUTES.ROOT)refetch();
    },[location.pathname]);
    useEffect(()=>{
        if(!loading && !data?.me){
            history.push(ROUTES.LOGIN);
        }else if(!loading && !error && data?.me){
            history.push(ROUTES.USER);
        }
    },[loading,error,data]);
    return (
        <Switch>
            <Route path={ROUTES.USER} exact><Account /></Route>
            <Route path={ROUTES.LOGIN} exact><Login /></Route>
            <Route path={ROUTES.ROOT}><div>loading...</div></Route>
        </Switch>
    );
}
