import React, { memo, ReactElement, useState } from 'react';
import { Input, PageWrapper, Button } from '../../../components';
import {
  useValidation,
  validateEmail,
  validatePassword,
  useLogin,
} from '../../../utils';
import './Login.scss';

export type LoginProps = {};

function Login(): ReactElement {
  const { value: email, setValue: setEmail, error: emailError } = useValidation(
    validateEmail
  );
  const {
    value: password,
    setValue: setPassword,
    error: passError,
  } = useValidation(validatePassword);
  const [showErrors, setShowErrors] = useState(false);
  const { login, serverError } = useLogin();
  const submit = (): void => {
    if (emailError || passError) {
      setShowErrors(true);
    } else {
      login({ email, password });
    }
  };

  return (
    <PageWrapper className="Login">
      <div className="innerWrapper">
        <strong>Login</strong>
        <div className="inputWrapper">
          <Input placeholder="email" onChange={setEmail} value={email} />
          <Input
            placeholder="password"
            onChange={setPassword}
            value={password}
          />
        </div>
        {showErrors ? (
          <>
            {emailError && <div className="error">Email not valid</div>}
            {passError && <div className="error">Password not valid</div>}
            {serverError && <div className="error">{serverError}</div>}
          </>
        ) : null}
        <Button onClick={submit}>Login</Button>
      </div>
    </PageWrapper>
  );
}

export default memo(Login);
