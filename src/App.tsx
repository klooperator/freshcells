import React, { ReactElement } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ApolloClient, InMemoryCache,ApolloProvider,createHttpLink,from } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

import Routes from './routes';
import {GRAPHQL} from './consts';

const httpLink = createHttpLink({
  uri: GRAPHQL.URL,
});

const contextLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem(GRAPHQL.TOKEN_KEY);
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  };
});


const client = new ApolloClient({
  link:from([
    contextLink,httpLink
  ]),
  cache: new InMemoryCache()
});




function App():ReactElement {
  return (
    <div className="App">
      <Router>
      <ApolloProvider client={client}>
        <Routes />
      </ApolloProvider>
        
      </Router>
    </div>
  );
}

export default App;
