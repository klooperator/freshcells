/* eslint-disable import/prefer-default-export */
import { gql } from '@apollo/client';

export interface IBaseUser {
  id: number;
  email: string;
  username: string;
  confirmed: boolean;
  blocked: boolean;
}

export interface IUser extends IBaseUser {
  firstName: string;
  lastName: string;
}

export const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      email
      firstName
      lastName
    }
  }
`;

export const GET_ME = gql`
  query me {
    me {
      id
      username
      confirmed
      email
      blocked
    }
  }
`;
