import { gql } from '@apollo/client';
import { IBaseUser } from '../queries/user';

export interface ILogin {
  identifier: string;
  password: string;
}

export interface ILoginReturn {
  jwt: string;
  user: IBaseUser;
}

export const LOGIN = gql`
  mutation login($identifier: String!, $password: String!) {
    login(input: { identifier: $identifier, password: $password }) {
      jwt
    }
  }
`;
