/* eslint-disable import/prefer-default-export */
export enum ROUTES {
  ROOT = '/',
  USER = '/user',
  LOGIN = '/login',
}
