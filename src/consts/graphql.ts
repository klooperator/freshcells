export enum GRAPHQL {
  URL = 'https://cms.trial-task.k8s.ext.fcse.io/graphql',
  TOKEN_KEY = 'graphql_super_secret_jwt_token',
}
