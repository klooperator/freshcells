import { validateEmail, validatePassword } from './validation';

describe('Utils tests', () => {
  test('Password validator', () => {
    // only thing that is validated is min char length
    expect(validatePassword('1234567')).toBe(false);
    expect(validatePassword('123456789')).toBe(true);
  });
  test('Email validation', () => {
    expect(validateEmail('1234567')).toBe(false);
    expect(validateEmail('aa@gmail')).toBe(false);
    expect(validateEmail('@gmail.com')).toBe(false);
    expect(validateEmail('aa@gmial.com')).toBe(true);
    expect(validateEmail('aa@aa@.com')).toBe(false);
    expect(validateEmail('aa@aa@a.com')).toBe(false);
  });
});
