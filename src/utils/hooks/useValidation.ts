import { useState, useEffect } from 'react';

export type UseValidationReturns = {
  value: string;
  setValue: (v: string) => void;
  error: boolean;
};

export function useValidation(
  validation: (v: string) => boolean
): UseValidationReturns {
  const [value, setValue] = useState('');
  const [error, setError] = useState(false);
  useEffect(() => {
    setError(!validation(value));
  }, [value]);

  return {
    value,
    setValue,
    error,
  };
}
