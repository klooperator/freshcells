import { useMutation } from '@apollo/client';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { GRAPHQL, ROUTES } from 'src/consts';
import { LOGIN } from 'src/graphql';
import { useLocalStorage } from '.';

export type SubmitArgs = {
  email: string;
  password: string;
};
export type UseLoginReturn = {
  login: (args: SubmitArgs) => void;
  serverError: string;
};

export function useLogin(): UseLoginReturn {
  const { setValue: setToken } = useLocalStorage(GRAPHQL.TOKEN_KEY);
  const history = useHistory();

  const [serverError, setServerErr] = useState('');

  const [login, { data }] = useMutation(LOGIN, {
    onError: (): void => {
      setServerErr('Invalid username or password, or other server error !!');
    },
  });

  useEffect(() => {
    if (data?.login?.jwt) {
      setToken(data.login.jwt);
      history.push(ROUTES.ROOT);
    }
  }, [data]);

  const submit = ({ email, password }: SubmitArgs): void => {
    login({ variables: { identifier: email, password } });
  };
  return {
    serverError,
    login: submit,
  };
}
