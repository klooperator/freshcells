import { useApolloClient } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { GRAPHQL, ROUTES } from '../../consts';
import { useLocalStorage } from './useLocalStorage';

export type UseLogoutReturn = {
  logout: () => void;
};

export function useLogout(): UseLogoutReturn {
  const { deleteValue: deleteToken } = useLocalStorage(GRAPHQL.TOKEN_KEY);
  const history = useHistory();

  const client = useApolloClient();

  const logout = async (): Promise<void> => {
    deleteToken();
    client.clearStore();
    history.push(ROUTES.ROOT);
  };
  return {
    logout,
  };
}
