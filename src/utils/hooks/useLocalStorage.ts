export type UseLocalStorageReturn = {
  value: string | null;
  setValue: (newValue: string) => void;
  deleteValue;
};
export function useLocalStorage(key: string): UseLocalStorageReturn {
  // initial idea was to attach listener here and update value on change
  // thats why hook...
  return {
    value: localStorage.getItem(key),
    setValue: (newValue): void => localStorage.setItem(key, newValue),
    deleteValue: (): void => localStorage.removeItem(key),
  };
}
